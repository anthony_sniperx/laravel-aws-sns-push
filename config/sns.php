<?php

return [
    'key'   =>  env('AWS_KEY'),
    'secret'   =>  env('AWS_SECRET'),
    'region'   =>  env('AWS_REGION'),
    'arn' => [
        'ios' => env('APPLICATION_ARN_IOS'),
        'android' => env('APPLICATION_ARN_ANDROID')
    ]
];
