<?php

namespace WebartDesign\SnsPush;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface HasPushTokens
{
    /**
     * @return MorphMany
     */
    public function push_tokens();

    /**
     * @return array
     */
    public function routeNotificationForSnsPush();
}
