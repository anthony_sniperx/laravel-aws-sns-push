<?php

namespace WebartDesign\SnsPush;

use Aws\Sns\SnsClient;

class Helpers
{
    public static function sns_client()
    {
        return new SnsClient([
            'credentials' => [
                'key' => config('sns.key'),
                'secret' => config('sns.secret')
            ],
            'region' => config('sns.region'),
            'version' => '2010-03-31'
        ]);
    }
}
