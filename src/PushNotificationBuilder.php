<?php

namespace WebartDesign\SnsPush;

class PushNotificationBuilder
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;

    /**
     * @var array
     */
    private $data;

    /**
     * @var string|null
     */
    private $sound = 'notification.wav';

    public function __construct($title = null, $body = null, $data = [])
    {
        $this->setTitle($title);
        $this->setBody($body);
        $this->setData($data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return PushNotificationBuilder
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param string $title
     * @return PushNotificationBuilder
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $body
     * @return PushNotificationBuilder
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param null|string $sound
     * @return PushNotificationBuilder
     */
    public function setSound($sound)
    {
        $this->sound = $sound;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSound()
    {
        return $this->sound;
    }
}
