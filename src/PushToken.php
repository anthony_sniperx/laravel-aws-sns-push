<?php

namespace WebartDesign\SnsPush;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class PushToken extends Model
{
    protected $guarded = [];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
