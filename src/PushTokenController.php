<?php

namespace WebartDesign\SnsPush;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PushTokenController extends Controller
{
    use ValidatesRequests;

    /**
     * Save a new push token.
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'device_type' => 'required|string'
        ]);
        $push_token = PushToken::query()
            ->where('token', $request->get('token'))
            ->first();
        if (!$push_token) {
            switch ($request->get('device_type')) {
                case 'ios':
                    $application_arn = config('sns.arn.ios');
                    break;
                case 'android':
                    $application_arn = config('sns.arn.android');
                    break;
                default:
                    throw new \Exception('No arn supplied for ' . $request->get('device_type'));
                    break;
            }
            $client = Helpers::sns_client();
            $result = $client->createPlatformEndpoint([
                'PlatformApplicationArn' => $application_arn,
                'Token' => $request->get('token'),
            ]);
            $request->user()->push_tokens()->create([
                'token' => $request->get('token'),
                'device_type' => $request->get('device_type'),
                'arn' => isset($result['EndpointArn']) ? $result['EndpointArn'] : ''
            ]);
        } else {
            $push_token->user_id = $request->user()->id;
            $push_token->save();
        }
    }

    /**
     * Delete push token by token.
     * @param Request $request
     * @param string $token
     * @return void
     */
    public function delete(Request $request, $token)
    {
        $request->user()->push_tokens()->where('token', $token)->delete();
    }
}
