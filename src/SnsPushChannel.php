<?php namespace WebartDesign\SnsPush;

use Aws\Sns\Exception\SnsException;
use Illuminate\Support\Facades\Log;

class SnsPushChannel
{
    /**
     * @param mixed $notifiable
     * @param SnsPushNotification $notification
     */
    public function send($notifiable, SnsPushNotification $notification)
    {
        if (!$push_tokens = $notifiable->routeNotificationFor('sns_push')) {
            return;
        }
        $builder = $notification->toSnsPush($notifiable);
        foreach ($push_tokens as $push_token) {
            if (empty($push_token->arn)) {
                continue;
            }
            $endPointArn = ["EndpointArn" => $push_token->arn];
            try {
                $sns = Helpers::sns_client();
                $endpointAtt = $sns->getEndpointAttributes($endPointArn);
                if ($endpointAtt != 'failed' && $endpointAtt['Attributes']['Enabled'] != 'false') {
                    $message = [
                        'default' => $builder->getBody()
                    ];
                    switch ($push_token->device_type) {
                        case 'android':
                            $message['GCM'] = \GuzzleHttp\json_encode(
                                [
                                    "notification" =>
                                        [
                                            "title" => $builder->getTitle(),
                                            "body" => $builder->getBody(),
                                            "sound" => $builder->getSound(),
                                        ],
                                    "data" => $builder->getData()
                                ]
                            );
                            break;
                        case 'ios':
                            $message['APNS'] = \GuzzleHttp\json_encode([
                                'aps' => [
                                    'alert' => [
                                        'title' => $builder->getTitle(),
                                        'body' => $builder->getBody()
                                    ],
                                    'sound' => $builder->getSound()
                                ],
                                'data' => $builder->getData()
                            ]);
                            break;
                        default:
                            continue;
                    }
                    $sns->publish([
                        'TargetArn' => $push_token->arn,
                        'Message' => \GuzzleHttp\json_encode($message),
                        'MessageStructure' => 'json'
                    ]);
                } else {
                    Log::error('Failed getting endpoint attributes.', $endpointAtt->toArray());
                }
            } catch (SnsException $e) {
                Log::error($e->getMessage());
            }
        }
    }
}
