<?php

namespace WebartDesign\SnsPush;

interface SnsPushNotification
{
    /**
     * @param $notifiable
     * @return PushNotificationBuilder
     */
    public function toSnsPush($notifiable);
}
